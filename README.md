***Project Title:*** **Urban Transport Mapping for Resilience in Kinshasa**


***Data collected by:*** GoMetro


***Funding:*** GFDRR (the Global Facility for Disaster Reduction and Recovery) and The World Bank


***Project Aim:*** The project aims to collect data during “dry” and “flooded” conditions to develop an understanding of the accessibility to jobs and other opportunities in Kinshasa during the wet and dry seasons.

***Data Collection Period:***

   ***- Dry Condition*** - The data collection exercise for the dry condition took place over 3 weeks from the ***30th of January to the 25th of February 2020.***

   ***- Wet Condition*** - The data collection exercise for the wet condition took place over 3 weeks from the ***5th of March to the 22nd of March 2020*** in order to cover all available modes of transport after heavy rains. 